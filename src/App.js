import './App.css';
import './app/Main_ShoeShop'
import Main_ShoeShop from './app/Main_ShoeShop';

function App() {
  return (
    <div className="App">

      <Main_ShoeShop />
    </div>
  );
}

export default App;
