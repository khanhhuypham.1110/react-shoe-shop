import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ADD_TO_CART, CLEAR_CART, DELETE_CART_ITEM, REMOVE_FROM_CART } from './Redux/Constant/ShoeConstant';


class Cart extends Component {
    renderTbody() {
        return (this.props.cart.map((item) => {
            return (
                <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price * item.number}</td>
                    <td>
                        <button style={{ backgroundColor: "white", border: "none", outline: "0" }}
                            onClick={() => this.props.handleRemoveFromCart(item)}>
                            -
                        </button>
                        {item.number}
                        <button style={{ backgroundColor: "white", border: "none", outline: "0" }}
                            onClick={() => this.props.handleAddToCart(item)}>+</button>
                    </td>
                    <td>
                        <img src={item.image} alt={item.name} style={{ width: "80px" }} />
                    </td>
                    <td><button className='btn btn-danger' onClick={() => this.props.handleDeleteCartItem(item)}><i class="fa-solid fa-trash"></i></button></td>
                </tr>
            )
        }))

    }
    render() {
        return (
            <table className='table'>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Image</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderTbody()}
                </tbody>
            </table>
        )
    }
}



let mapStateToProps = (state) => {
    return {
        cart: state.shoeReducer.cart
    }

}

let mapDispatchToProps = (dispatch) => {
    return {
        handleAddToCart: (item) => {
            dispatch({
                type: ADD_TO_CART,
                payload: item
            })
        },

        handleRemoveFromCart: (item) => {
            dispatch({
                type: REMOVE_FROM_CART,
                payload: item
            })
        },

        handleDeleteCartItem: (item) => {
            dispatch({
                type: DELETE_CART_ITEM,
                payload: item
            })
        },

        handleClearCart: () => {
            dispatch({
                type: CLEAR_CART,
            })
        }
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(Cart)


