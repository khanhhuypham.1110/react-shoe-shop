import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ADD_TO_CART, VIEW_DETAIL } from './Redux/Constant/ShoeConstant';


class ItemShoe extends Component {
    render() {
        let { image, name } = this.props.data
        return (
            <div className='col-3 p-1'>
                <div className=" card text-left h-100">
                    <img className="card-img-top" src={image} alt={name} style={{ height: "250px" }} />
                    <div className="card-body">
                        <h4 className="card-title" style={{ height: "60px" }}>{name}</h4>
                        <button className='btn btn-success mr-4' onClick={() => this.props.handleAddToCart(this.props.data)}>Buy</button>
                        <button className='btn btn-primary' onClick={() => this.props.handleViewDetail(this.props.data)}>Detail</button>
                    </div>
                </div>
            </div>
        )
    }
}

let mapStateToProps = (state) => {

}

let mapDispatchToProps = (dispatch) => {
    return {
        handleAddToCart: (item) => {
            dispatch({
                type: ADD_TO_CART,
                payload: item
            })
        },

        handleViewDetail: (item) => {
            dispatch({
                type: VIEW_DETAIL,
                payload: item
            })
        }


    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ItemShoe)


