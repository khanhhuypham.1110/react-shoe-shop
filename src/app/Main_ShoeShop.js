import React, { Component } from 'react'
import Cart from './Cart'
import DetailShoe from './DetailShoe'
import ListShoe from './ListShoe'

export default class Main_ShoeShop extends Component {
    render() {
        return (
            <div className='container'>
                <Cart />
                <ListShoe />
                <DetailShoe />
            </div>
        )
    }
}
