import React, { Component } from 'react'
import { connect } from 'react-redux'
import ItemShoe from './ItemShoe'
import { SEARCH_SHOE, SORT_SHOE } from './Redux/Constant/ShoeConstant'

class ListShoe extends Component {

    renderSearchBar = () => {
        return (
            <div className="d-flex justify-content-start w-50">
                <div className="input-group mb-3 mr-4 w-50">
                    <select class="custom-select" id="inputGroupOfSortName">
                        <option selected>Choose...</option>
                        <option value="A-Z">A-Z</option>
                        <option value="Z-A">Z-A</option>
                        <option value="LOW-TO-HIGH">Price: Low to High</option>
                        <option value="HIGH-TO-LOW">Price: High to low</option>
                    </select>
                </div>
                <div className="w-50">
                    <div className="d-flex justify-content-end my-2 my-lg-0">
                        <input className="form-control" type="search" placeholder="Search" aria-label="Search" id="searchInput" />
                        <button className="btn btn-outline-success my-2 my-sm-0" onClick={() => this.props.handleSearch()}>Search</button>
                    </div>
                </div>

            </div>
        )
    }

    renderSearchArr = () => {
        return this.props.searchArr.map((item, index) => {
            return <ItemShoe key={index} data={item} />
        })
    }

    renderShoeList = () => {
        return this.props.list.map((item) => {
            return <ItemShoe data={item} />
        })
    }
    render() {
        if (this.props.searchArr.length > 0) {
            return (
                <div>
                    <div className="mb-4">{this.renderSearchBar()}</div>
                    <div className="row">{this.renderSearchArr()}</div>
                </div>
            )
        } else {
            return (
                <div>
                    <div className="mb-4">{this.renderSearchBar()}</div>
                    <div className="row">{this.renderShoeList()}</div>
                </div>
            )
        }
    }
}


let mapStateToProps = (state) => {
    return {
        list: state.shoeReducer.shoeArr,
        searchArr: state.shoeReducer.searchArr
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handleSort: () => {
            dispatch({
                type: SORT_SHOE,
                payload: document.getElementById("inputGroupOfSortName").value
            })
        },
        handleSearch: () => {
            dispatch({
                type: SEARCH_SHOE,
                payload: document.getElementById("searchInput").value
            })

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListShoe)