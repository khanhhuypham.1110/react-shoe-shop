import React, { Component } from 'react'
import { connect } from 'react-redux'




class DetailShoe extends Component {
    render() {
        let { name, image, price, description } = this.props.detail
        return (
            <div className='row'>
                <img src={image} className="col-4" alt={name} />
                <div className='col-8'>
                    <p>{name}</p>
                    <p>{price}</p>
                    <p>{description}</p>
                </div>
            </div>
        )
    }
}


let mapStateToProps = (state) => {
    return {
        detail: state.shoeReducer.detail,
    }

}

let mapDispatchToProps = (dispatch) => {
    return {}
}


export default connect(mapStateToProps, mapDispatchToProps)(DetailShoe)