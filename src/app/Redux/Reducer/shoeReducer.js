import { dataShoe } from '../../dataShoe'
import { ADD_TO_CART, DELETE_CART_ITEM, REMOVE_FROM_CART, SEARCH_SHOE, VIEW_DETAIL } from '../Constant/ShoeConstant'

let initialState = {
    shoeArr: dataShoe,
    searchArr: [],
    detail: dataShoe[0],
    cart: [],
}

export const shoeReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_CART: {
            let cloneCart = [...state.cart]
            let index = cloneCart.findIndex((item) => item.id === action.payload.id)
            if (index === -1) {
                //if the added item doesn't exist in cart, we create a new item having quantity, which we
                // add to the cart
                let newItem = { ...action.payload, number: 1 }
                cloneCart.push(newItem)
            } else {
                cloneCart[index].number += 1
            }
            return { ...state, cart: cloneCart }
        }
        case REMOVE_FROM_CART: {
            let cloneCart = [...state.cart]
            let index = cloneCart.findIndex((item) => item.id === action.payload.id)
            if (index === -1) {
                return
            } else {
                if (cloneCart[index].number <= 1) {
                    cloneCart.splice(index, 1)
                    return { ...state, cart: cloneCart }
                }
                cloneCart[index].number -= 1
            }

            return { ...state, cart: cloneCart }
        }
        case DELETE_CART_ITEM: {
            let cloneCart = [...state.cart]
            let index = cloneCart.findIndex((item) => item.id === action.payload.id)
            if (index === -1) {
                return { ...state, cart: cloneCart }
            } else {
                cloneCart.splice(index, 1)
            }
            return { ...state, cart: cloneCart }
        }
        case VIEW_DETAIL: {
            return { ...state, detail: action.payload }
        }

        case SEARCH_SHOE: {
            let cloneSearchArr = [...state.searchArr]
            let keyword = action.payload.toLowerCase().trim()
            state.shoeArr.forEach((item) => {
                let comparedStr = item.name.toLowerCase().trim()
                console.log(comparedStr.includes(keyword));
                if (comparedStr.includes(keyword)) {
                    cloneSearchArr.push(item)
                }
            })
            return { ...state, searchArr: cloneSearchArr }
        }

        default:
            return state
    }
}
